import ProductsReducer from "./productReducer/ProductReducer";
import CartProductsReducer from "./ProductSort/CartProductsReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  ProductsReducer,
  CartProductsReducer,
});

export default rootReducer;
