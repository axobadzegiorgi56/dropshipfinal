import {
  PRODUCTS_CHECKED,
  PRODUCTS_CHECKED_ALL,
  PRODUCTS_FETCHED,
  PRODUCTS_SEARCHED,
  PRODUCTS_SORTED,
  PRODUCTS_UNCHECKED,
  MODAL_OPEN,
  EDITOR_OPEN,
  PRODUCTS_FILTERED,
} from "./ActionTypes";

export const getProductsAction = (products) => {
  return {
    type: PRODUCTS_FETCHED,
    payload: products,
  };
};

export const productsSortAction = (products) => {
  return {
    type: PRODUCTS_SORTED,
    payload: products,
  };
};

export const productsFilterAction = (products) => {
  return {
    type: PRODUCTS_FILTERED,
    payload: products,
  };
};

export const checkedProductsAction = (products) => {
  return {
    type: PRODUCTS_CHECKED,
    payload: products,
  };
};

export const uncheckedProductsAction = (products) => {
  return {
    type: PRODUCTS_UNCHECKED,
    payload: products,
  };
};

export const CheckAllProductsAction = (products) => {
  return {
    type: PRODUCTS_CHECKED_ALL,
    payload: products,
  };
};

export const searchedProductsAction = (products) => {
  return {
    type: PRODUCTS_SEARCHED,
    payload: products,
  };
};

export const modalOpenAction = (bool) => {
  return {
    type: MODAL_OPEN,
    payload: bool,
  };
};

export const editorOpenAction = (bool) => {
  return {
    type: EDITOR_OPEN,
    payload: bool,
  };
};
