import {
  PRODUCTS_CHECKED,
  PRODUCTS_CHECKED_ALL,
  PRODUCTS_DISPLAYED,
  PRODUCTS_FETCHED,
  PRODUCTS_SEARCHED,
  PRODUCTS_SORTED,
  PRODUCTS_UNCHECKED,
  MODAL_OPEN,
  EDITOR_OPEN,
  PRODUCTS_FILTERED,
} from "./ActionTypes";

const initState = {
  productList: [],
  displayedProductList: [],
  checkedProductList: [],
  modalState: false,
  editorState: false,
};

const ProductsReducer = (state = initState, action) => {
  console.log("Products Reducer Returned", state, action);
  switch (action.type) {
    case PRODUCTS_FETCHED:
      return {
        ...state,
        productList: action.payload,
        displayedProductList: action.payload,
      };
    case PRODUCTS_SORTED:
      return { ...state, displayedProductList: action.payload };
    case PRODUCTS_DISPLAYED:
      return { ...state, displayedProductList: action.payload };
    case PRODUCTS_SEARCHED:
      return { ...state, displayedProductList: action.payload };
    case PRODUCTS_CHECKED:
      return {
        ...state,
        checkedProductList: [...state.checkedProductList, action.payload],
      };
    case PRODUCTS_UNCHECKED:
      return {
        ...state,
        checkedProductList: [
          ...state.checkedProductList.filter((item) => item !== action.payload),
        ],
      };
    case PRODUCTS_CHECKED_ALL:
      return {
        ...state,
        checkedProductList: [...action.payload],
      };
    case MODAL_OPEN:
      return { ...state, modalState: action.payload };
    case EDITOR_OPEN:
      return { ...state, editorState: action.payload };
    case PRODUCTS_FILTERED:
      return { ...state, displayedProductList: action.payload };
    default:
      return { ...state };
  }
};

export default ProductsReducer;
