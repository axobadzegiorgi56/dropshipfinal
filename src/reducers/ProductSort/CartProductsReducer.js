import { CART_PRODUCTS_FETCHED } from "../productReducer/ActionTypes";

const initState = {
  productSortedList: [],
};

const CartProductsReducer = (state = initState, action) => {
  console.log("Sort Reducer Returned", state, action);
  switch (action.type) {
    case CART_PRODUCTS_FETCHED:
      return { ...state, productSortedList: action.payload };
    default:
      return { ...state };
  }
};

export default CartProductsReducer;
