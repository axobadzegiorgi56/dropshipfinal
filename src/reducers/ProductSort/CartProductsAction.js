export const getCartProductsAction = (products) => {
  return {
    type: "CART_PRODUCTS_FETCHED",
    payload: products,
  };
};
