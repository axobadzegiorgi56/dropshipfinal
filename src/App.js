import "./App.css";
import Main from "./components/main";
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SignupPage from "./components/register";
import LoginPage from "./components/login";
import Cart from "./components/cart";
import Catalog from "./components/catalog";
import Profile from "./components/profile";
function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Main />
          </Route>
          <Route path="/cart">
            <Cart />
          </Route>
          <Route path="/catalog/:productId?">
            <Catalog />
          </Route>
          <Route path="/auth/signup">
            <SignupPage />
          </Route>
          <Route path="/auth/login">
            <LoginPage />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
