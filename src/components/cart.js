import { useEffect, useState } from "react";
import { cartFetch, removeFromCart } from "./APInotyet";
import Navigation from "./navigation";
import "./cart.css";
import DeleteIcon from "../images/delete-icon.PNG";
import { Snackbar } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

const Cart = () => {
  const [cartProducts, setCartProducts] = useState([]);
  useEffect(() => {
    cartFetch().then((res) => {
      setCartProducts(res);
      console.log(res);
    });
    console.log(cartProducts);
  }, []);

  const removeItem = async (id, name) => {
    await removeFromCart(id);
    await cartFetch().then((res) => setCartProducts(res));
    setSnackBarOpen(true);
  };

  const [snackBarOpen, setSnackBarOpen] = useState(false);
  const [snackBarMessage, setSnackBarMessage] = useState("");

  const removeItemAlertClose = () => {
    setSnackBarOpen(false);
  };

  return (
    <div className="cart">
      <Navigation />
      <div className="cart__main">
        <Snackbar
          open={snackBarOpen}
          autoHideDuration={3000}
          onClose={removeItemAlertClose}
        >
          <Alert onClose={removeItemAlertClose} severity="success">
            Product has been removed from your cart!
          </Alert>
        </Snackbar>
        <div className="cart__header">
          <span>SHOPPING CART ({cartProducts.length})</span>
        </div>
        <div className="table__container">
          <div className="table">
            <div className="table__header">
              <span className="table__description">ITEM DESCRIPTION</span>
              <span className="table__supplier">SUPPLIER</span>
              <span>OPTIONS</span>
              <span>QUANTITY</span>
              <span>ITEM COST</span>
              <span>VAT</span>
              <span>TOTAL COST</span>
              <span></span>
            </div>
            <div className="table__main">
              {cartProducts.map((item) => (
                <div
                  key={item.id}
                  className="table__item"
                  onClick={() => console.log(item.id)}
                >
                  <div className="item__description">
                    <img src={item.image} alt="itemimage"></img>
                    <div className="item__title">
                      <span className="item-name">{item.title}</span>
                      <span className="item-code">SKU# bgb-s0586562</span>
                    </div>
                  </div>

                  <div className="item__supplier">
                    <span>SB-Supplier115</span>
                  </div>
                  <div className="item__options"></div>
                  <div className="item__qty">
                    <div className="qty">
                      <button onClick={console.log("minus")}>-</button>
                      <input value={item.qty}></input>
                      <button>+</button>
                    </div>
                  </div>

                  <div className="item__cost">${item.price}</div>

                  <div className="item__vat">$0.00</div>

                  <div className="item__total-cost">
                    ${item.price * item.qty}
                  </div>
                  <div className="item__delete">
                    <img
                      alt="deleteIcon"
                      src={DeleteIcon}
                      onClick={() => {
                        removeItem(item.id, item.title);
                      }}
                    ></img>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
