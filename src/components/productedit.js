import "./productedit.css";
import { useDispatch, useSelector } from "react-redux";
import {
  editorOpenAction,
  productsSortAction,
} from "../reducers/productReducer/ProductsAction";
import * as yup from "yup";
import { useEffect, useState } from "react";
import {
  singleProduct,
  updateProduct,
  addProduct,
  productsFetch,
} from "./APInotyet";
import { Form, Formik, Field, ErrorMessage } from "formik";
import InputAdornment from "@material-ui/core/InputAdornment";
import TitleIcon from "@material-ui/icons/Title";
import DescriptionIcon from "@material-ui/icons/Description";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import ImageIcon from "@material-ui/icons/Image";
import { Button, makeStyles, TextField } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const validationSchema = yup.object().shape({
  title: yup.string().required().min(2).max(100),
  description: yup.string().required().min(5).max(1000),
  price: yup.number().integer().required().min(2),
  imageUrl: yup.string().required().url(),
});

const useStyles = makeStyles({
  root: {
    borderRadius: "4",
    marginTop: "15px",
    marginBottom: "5px",
  },
  dialogButtonSave: {
    marginTop: "15px",
    width: "140px",
    height: "50px",
    backgroundColor: "#61d5df",
    color: "#fff",
    border: "none",
    borderRadius: "4px",
    fontWeight: "600",
    fontSize: "12px",
    cursor: "pointer",
  },
  descriptionInputField: {
    width: "100%",
    // height: "150px",
    // color: "#61d4df",
    // margin: "35px 0 170px 0",
    marginTop: "15px",
    marginBottom: "5px",
    // height: 50,
  },
  formField: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    height: "100%",
    // marginBottom: "5px",
    marginTop: "40px",
  },
});

const ProductEdit = () => {
  const classes = useStyles();
  const editorState = useSelector((state) => state.ProductsReducer.editorState);
  const dispatch = useDispatch();
  const [product, setProduct] = useState({});
  useEffect(() => {
    if (typeof editorState !== "boolean") {
      singleProduct(editorState).then((res) => {
        setProduct(res);
      });
    }
  }, [editorState]);

  const handleSubmit = (values) => {
    if (typeof editorState !== "boolean") {
      updateProduct(editorState, values)
        .then((res) => {
          dispatch(editorOpenAction(false));
          setProduct({});
          productsFetch().then((result) => {
            dispatch(productsSortAction(result));
          });
        })
        .catch((err) => {
          alert(err.message);
        });
    } else {
      addProduct(values)
        .then((res) => {
          dispatch(editorOpenAction(false));
          setProduct({});
          productsFetch().then((result) => {
            dispatch(productsSortAction(result));
          });
        })
        .catch((err) => {
          alert(err.message);
        });
    }
  };

  const customInputTitle = ({ field, form: { touched, errors }, ...props }) => {
    return (
      <TextField
        placeholder="Title"
        name="edit-title"
        error={touched.title && errors.title ? true : false}
        className={classes.root}
        fullWidth
        variant="outlined"
        id="input-with-icon-textField"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <TitleIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customInputPrice = ({ field, form: { touched, errors }, ...props }) => {
    return (
      <TextField
        error={touched.price && errors.price ? true : false}
        className={classes.root}
        fullWidth
        variant="outlined"
        id="input-with-icon-textField"
        placeholder="Price"
        name="edit-price"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <AttachMoneyIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customInputImageUrl = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        name="edit-imageurl"
        error={touched.imageUrl && errors.imageUrl ? true : false}
        className={classes.root}
        fullWidth
        variant="outlined"
        id="input-with-icon-textField"
        placeholder="Image URL"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <ImageIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const customInputDescription = ({
    field,
    form: { touched, errors },
    ...props
  }) => {
    return (
      <TextField
        error={touched.description && errors.description ? true : false}
        className={classes.descriptionInputField}
        variant="outlined"
        id="input-with-icon-textField"
        multiline
        rows={5}
        placeholder="Description"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <DescriptionIcon style={{ color: "#61d5df" }} />
            </InputAdornment>
          ),
        }}
        {...field}
        {...props}
      />
    );
  };

  const closeEditor = () => {
    dispatch(editorOpenAction(false));
    setProduct({});
  };

  return (
    <div
      className={`editor__modal ${
        editorState ? " editor__modal--visible" : ""
      }`}
    >
      <div className="editor__dialog">
        <div className="dialog__content">
          <div className="dialog__content-left">
            <div className="dialog__img-container">
              <img src={product && product.imageUrl} />
            </div>
          </div>
          <div className="dialog__content-right">
            <div className="dialog-close-btn" onClick={closeEditor}>
              <CloseIcon></CloseIcon>
            </div>
            <Formik
              enableReinitialize
              initialValues={
                typeof editorState !== "boolean"
                  ? {
                      title: product.title,
                      description: product.description,
                      price: product.price,
                      imageUrl: product.imageUrl,
                    }
                  : {
                      title: "",
                      description: "",
                      price: "",
                      imageUrl: "",
                    }
              }
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              <Form className={classes.formField}>
                <div className="dialog__content-field">
                  <span>Product Title:</span>
                  <Field name="title" component={customInputTitle} />
                  <ErrorMessage
                    className="errorMessage"
                    name="title"
                    component="div"
                  />
                </div>
                <div className="dialog__content-field">
                  <span>Product Price:</span>
                  <Field name="price" component={customInputPrice} />
                  <ErrorMessage
                    className="errorMessage"
                    name="price"
                    component="div"
                  />
                </div>
                <div className="dialog__content-field">
                  <span>Product Image URL:</span>
                  <Field name="imageUrl" component={customInputImageUrl} />
                  <ErrorMessage
                    className="errorMessage"
                    name="imageUrl"
                    component="div"
                  />
                </div>
                <div className="dialog__content-field description-field">
                  <span>Product Description:</span>
                  <Field
                    name="description"
                    component={customInputDescription}
                    className={classes.descriptionInputField}
                  />
                  <ErrorMessage
                    className="errorMessage"
                    name="description"
                    component="div"
                  />
                </div>
                <Button
                  variant="contained"
                  className={classes.dialogButtonSave}
                  type="submit"
                >
                  Save Changes
                </Button>
              </Form>
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductEdit;
