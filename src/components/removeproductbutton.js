import "./DeleteProductButton.css";
import deleteProductIcon from "../../../assets/delete.png";
import { deleteProduct as deleteProductAPI, products } from "./APInotyet";
import { useSnackbar } from "notistack";
import { useDispatch } from "react-redux";

const DeleteProductButton = ({ id }) => {
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  let variant = "success";

  const deleteProduct = () => {
    deleteProduct(id).then(() => {
      products().then((result) => {
        dispatch(catalogItemsRequestAction(result));
      });
    });
  };

  return (
    <button onClick={deleteProduct}>
      <img src={deleteProductIcon} alt="Delete Product" />
    </button>
  );
};

export default DeleteProductButton;
