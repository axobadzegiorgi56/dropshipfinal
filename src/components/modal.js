import "./modal.css";
import { singleProduct } from "./APInotyet";
import { useParams } from "react-router";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalOpenAction } from "../reducers/productReducer/ProductsAction";
import { useHistory } from "react-router-dom";
import CloseIcon from "@material-ui/icons/Close";
import { Slider, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  closebutton: {
    borderRadius: "4",
    marginTop: "15px",
    marginBottom: "5px",
  },
});

const Modal = () => {
  const history = useHistory();
  const modalState = useSelector((state) => state.ProductsReducer.modalState);
  const [openProduct, setOpenProduct] = useState({});
  const dispatch = useDispatch();
  const { productId } = useParams();
  useEffect(() => {
    if (productId !== undefined) {
      singleProduct(productId).then((res) => {
        setOpenProduct(res);
      });
    }
  }, [productId]);

  const closeModal = () => {
    setOpenProduct({});
    history.push("/catalog");
    dispatch(modalOpenAction(false));
  };

  return (
    <div
      className={`modal__container ${
        modalState ? " modal__container--show" : ""
      }`}
    >
      <div className="modal__filler" onClick={closeModal}></div>
      <div className="dialog">
        <div className="dialog__left">
          <div className="dialog__prices">
            <ul>
              <li>
                <strong>$4930</strong>
                <span>RRP</span>
              </li>
              <li>
                <strong>{openProduct.price}</strong>
                <span>cost</span>
              </li>
              <li>
                <strong className="profit">$4930</strong>
                <span>profit</span>
              </li>
            </ul>
          </div>
          <div className="dialog__image">
            <img src={openProduct.imageUrl} alt="dialogimage" />
          </div>
        </div>
        <div className="dialog__right">
          <div className="dialog__close-btn">
            <CloseIcon onClick={closeModal}></CloseIcon>
          </div>
          <div className="dialog__title">
            <div className="dialog__header">
              <span className="sku">SKU# bgb-s0430173 COPY</span>
              <h3>Supplier: SP-Supplier115</h3>
            </div>
          </div>
          <h2 className="title">{openProduct.title}</h2>
          <div className="dialog__buttons">
            <button>Add To My Inventory</button>
          </div>
          <div className="dialog__tabs">
            <div className="description__tab">Product Details</div>
          </div>
          <div className="dialog__description">
            <span>{openProduct.description}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
