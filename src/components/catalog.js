import { useEffect, useState } from "react";
import "./catalog.css";
import { addToCart, productsFetch, deleteProductAPI } from "./APInotyet";
import Navigation from "./navigation";
import searchButton from "../images/search-button.PNG";
import { useDispatch, useSelector } from "react-redux";
import {
  CheckAllProductsAction,
  checkedProductsAction,
  editorOpenAction,
  getProductsAction,
  modalOpenAction,
  productsSortAction,
  searchedProductsAction,
  uncheckedProductsAction,
} from "../reducers/productReducer/ProductsAction";
import Modal from "./modal";
import { useParams } from "react-router";
import { useHistory } from "react-router-dom";
import ProductEdit from "./productedit";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
// import { Snackbar } from "@material-ui/core";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
import { Snackbar } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
// import SliderBar from "./sliderbar";

const Catalog = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const products = useSelector((state) => state.ProductsReducer.productList);
  const { productId } = useParams();
  const checkedProductList = useSelector(
    (state) => state.ProductsReducer.checkedProductList
  );
  const [sortValue, setSortValue] = useState("");
  const displayedProducts = useSelector(
    (state) => state.ProductsReducer.displayedProductList
  );
  console.log("LSTSTSTST", checkedProductList);
  useEffect(() => {
    productsFetch().then((result) => {
      dispatch(getProductsAction(result));
    });
    if (productId !== undefined) {
      dispatch(modalOpenAction(true));
    }
  }, []);

  // SORT STUFF

  const ChangeValue = (e) => {
    setSortValue(e.target.value);
  };

  const [priceRange, setPriceRange] = useState([0, 1000]);

  const sortItems = (sortValue, products) => {
    if (sortValue === "desc") {
      products.sort((a, b) => b.price - a.price);
    }
    if (sortValue === "asc") {
      products.sort((a, b) => a.price - b.price);
    }
    if (sortValue === "InAlphabet") {
      products.sort((a, b) => {
        if (a.title > b.title) {
          return -1;
        }
      });
    } else if (sortValue === "Alphabet") {
      products.sort((a, b) => {
        if (a.title < b.title) {
          return -1;
        }
      });
    }

    return products;
  };

  useEffect(() => {
    if (sortValue) {
      const sortedItems = sortItems(sortValue, displayedProducts);
      dispatch(productsSortAction([...sortedItems]));
    }
  }, [sortValue]);

  // SELECT STUFF

  const selectAll = () => {
    dispatch(
      CheckAllProductsAction(displayedProducts.map((listItem) => listItem.id))
    );
  };

  const deselectAll = () => {
    dispatch(CheckAllProductsAction([]));
  };

  const selectItem = (id) => {
    const checked = checkedProductList.findIndex((item) => item === id);
    if (checked > -1) {
      dispatch(uncheckedProductsAction(id));
    } else {
      dispatch(checkedProductsAction(id));
    }
  };

  // SEARCH STUFF
  const [searchvalue, setSearchValue] = useState("");
  const search = () => {
    console.log(searchvalue, " search...");
    if (searchvalue) {
      dispatch(
        searchedProductsAction(
          sortItems(
            sortValue,
            products.filter((item) =>
              item.title.toLowerCase().includes(searchvalue)
            )
          )
        )
      );
    } else if (searchvalue === "") {
      dispatch(searchedProductsAction(sortItems(sortValue, products)));
    }
  };

  // MODAL STATE
  const openModal = (id) => {
    history.push(`/catalog/${id}`);
    dispatch(modalOpenAction(true));
  };

  // EDITOR STATE

  const openEditor = (id) => {
    dispatch(editorOpenAction(id));
  };
  const openAddProduct = () => {
    dispatch(editorOpenAction(true));
  };

  // DELETE PRODUCT

  //   const deleteProduct = (id) => {
  //     removeFromCart(id).then(() =>{
  //       productsFetch().then((result) => {
  //         dispatch(getProductsAction(result));
  //     })
  //   }
  // };

  const deleteProduct = async (id) => {
    console.log(id);
    await deleteProductAPI(id);
    await productsFetch().then((result) => {
      dispatch(getProductsAction(result));
    });
    setSnackBarMessage("Product Has Been Removed From Catalog");
    setSnackBarOpen(true);
  };

  //SNACKBAR STATE
  // const [snackBarOpen, setSnackBarOpen] = useState(false);

  // const addItemAlert = (value = false) => {
  //   setSnackBarOpen(value);
  // };

  const options = ["one", "two", "three"];
  const [snackBarOpen, setSnackBarOpen] = useState(false);
  const [snackBarMessage, setSnackBarMessage] = useState("");
  const removeItemAlertClose = () => {
    setSnackBarOpen(false);
  };

  return (
    <div className="catalog">
      <Snackbar
        open={snackBarOpen}
        autoHideDuration={3000}
        onClose={removeItemAlertClose}
      >
        <Alert onClose={removeItemAlertClose} severity="success">
          {snackBarMessage}
        </Alert>
      </Snackbar>
      <div className="catalog__wrapper">
        <div className="catalog__filters">
          <div className="filter__niche filter">
            <span>Choose Niche</span>
          </div>
          <div className="filter__category filter">
            <span>Choose Category</span>
          </div>
          <div className="dropdowns__container">
            <Dropdown
              options={options}
              className="filter__dropdown"
              placeholder="Ship From"
            />
            <Dropdown
              options={options}
              className="filter__dropdown"
              placeholder="Ship To"
            />
            <Dropdown
              options={options}
              className="filter__dropdown"
              placeholder="Select Supplier"
            />
          </div>
        </div>
        <div className="catalog__main">
          <header className="catalog__header">
            <div className="header__left">
              <button className="select_all-btn header-btn" onClick={selectAll}>
                Select all
              </button>
              <span className="seperate"></span>
              <span className="selected__amount">
                Selected {checkedProductList.length} out of{" "}
                {displayedProducts.length} products
              </span>
              <button
                className={`header-btn clear-btn ${
                  checkedProductList.length > 0 ? "visible" : "hidden"
                }`}
                onClick={deselectAll}
              >
                CLEAR SELECTED
              </button>
            </div>
            <div className="catalog__right">
              <button
                onClick={openAddProduct}
                className="header__product-add header-btn"
              >
                Add Product
              </button>
              <div className="catalog__input-holder">
                <div className="search-btn-container">
                  <img
                    alt="searchbutton"
                    className="search-button"
                    src={searchButton}
                    onClick={search}
                  />
                </div>
                <input
                  className="catalog__search"
                  type="text"
                  placeholder="Search..."
                  onChange={(e) => setSearchValue(e.target.value)}
                ></input>
              </div>
              <button className="inventory__add-btn header-btn">
                Add to inventory
              </button>
              <div className="help-menu">
                <button className="help-menu-btn">
                  <span>?</span>
                </button>
              </div>
            </div>
          </header>
          <div className="catalog__sort">
            <select
              onChange={ChangeValue}
              className="catalog__sort-select"
              placeholder="sort"
            >
              <option value="asc">PRICE: Low To High</option>
              <option value="desc">PRICE: High To Low</option>
              <option value="Alphabet">ALPHABET: A-Z</option>
              <option value="InAlphabet">ALPHABET: Z-A</option>
            </select>
          </div>
          <div className="catalog__products">
            {displayedProducts.map((item) => (
              <div
                key={item.id}
                className={`catalog__item                 
                ${
                  checkedProductList.includes(item.id)
                    ? " catalog__item--highlighted"
                    : ""
                }`}
              >
                <div className="item-hover-container">
                  <button
                    className="item-delete-btn"
                    onClick={() => deleteProduct(item.id)}
                  >
                    <DeleteIcon style={{ color: "#FFF" }} />
                  </button>
                  <button
                    className="item-edit-btn"
                    onClick={() => {
                      openEditor(item.id);
                    }}
                  >
                    <EditIcon style={{ color: "#FFF" }} />
                  </button>
                  <button
                    className="item-add-btn"
                    onClick={() => {
                      addToCart(item.id, 1);
                    }}
                  >
                    Add To Inventory
                  </button>
                </div>
                <input
                  type="checkbox"
                  className={`item-checkbox ${
                    checkedProductList.includes(item.id)
                      ? " item-checkbox-visible"
                      : ""
                  }`}
                  onChange={() => selectItem(item.id)}
                  checked={checkedProductList.includes(item.id)}
                ></input>
                <div
                  className="catalog__img-container"
                  onClick={() => openModal(item.id)}
                >
                  <img
                    className="catalog__img"
                    src={item.imageUrl}
                    alt="catalogimg"
                  />
                </div>
                <div
                  className="item__information"
                  onClick={() => openModal(item.id)}
                >
                  <div className="item__name-container">
                    <p className="item__name">{item.title}</p>
                  </div>
                  <p className="item__supplier">By: SP-Supplier115</p>
                  <p className="item__price">${item.price}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <Modal />
      <ProductEdit />
      <Navigation />
    </div>
  );
};

export default Catalog;
