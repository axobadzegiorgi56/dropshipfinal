import "./auth.css";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import keypng from "../images/key-auth.PNG";
import mailpng from "../images/mail-auth.PNG";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { useEffect } from "react";
import axios from "axios";
import React from "react";
import * as yup from "yup";
import { useFormik } from "formik";

const signUpValidation = yup.object({
  email: yup.string().required("Email Required").email("Invalid Email"),
  password: yup
    .string()
    .required("Password Required")
    .min(6, "Password must be Atleast 6 Characters"),
});

const SignUpPage = () => {
  const BASE_URL = "http://18.185.148.165:3000";
  let history = useHistory();

  const useStyles = makeStyles({
    root: {
      borderRadius: 4,
      width: "331px",
      height: "48px",
      marginBottom: "30px",
    },
    logbutton: {
      background: "#61d5df",
      border: 0,
      borderRadius: 4,
      color: "white",
      padding: "15px 30px",
      width: "333px",
      marginTop: "0px",
      fontFamily: "sans-serif",
      fontWeight: "500",
      "&:hover": {
        backgroundColor: "#61d5df",
      },
    },
    button: {
      background: "#61d5df",
      border: 0,
      borderRadius: 4,
      color: "white",
      padding: "15px 30px",
      width: "333px",
      marginTop: "20px",
      fontFamily: "sans-serif",
      fontWeight: "500",
      "&:hover": {
        backgroundColor: "#61d5df",
      },
    },
  });
  const classes = useStyles();

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      history.push("/catalog");
      console.log(token);
    }
  });

  const checkToken = (history) => {
    const token = localStorage.getItem("token");
    if (token) {
      history.push("/catalog");
      console.log(token);
    }
  };

  const registerForm = useFormik({
    initialValues: { email: "", password: "" },
    onSubmit: (values) => {
      Register(values.email, values.password).then(() => {
        checkToken(history);
      });
    },
    validationSchema: signUpValidation,
  });

  const Register = async (email, password) => {
    try {
      const result = await axios.post(BASE_URL + "/register", {
        password: password,
        firstName: "John",
        lastName: "Doe",
        email: email,
        passwordConfirmation: password,
      });
      if (result.data.success) {
        history.push("login");
        console.log(result);
      }
    } catch (err) {
      alert("something went wrong!");
    }
  };

  return (
    <div className="auth">
      <div className="auth__container">
        <div className="auth__heading">
          <div className="auth__logo">
            <img
              src="https://app.365dropship.com/assets/images/auth/logo.svg"
              alt="logo"
            />
          </div>
        </div>
        <p className="auth__title">Sign Up</p>
        <form className="auth__inputs" onSubmit={registerForm.handleSubmit}>
          <TextField
            error={
              registerForm.touched.email && Boolean(registerForm.errors.email)
            }
            value={registerForm.values.email}
            onChange={registerForm.handleChange}
            helperText={registerForm.touched.email && registerForm.errors.email}
            name="email"
            id="email"
            fullWidth
            variant="outlined"
            placeholder="E-mail"
            className={classes.root}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <img src={mailpng} alt="mail" />
                </InputAdornment>
              ),
            }}
          />

          <TextField
            error={
              registerForm.touched.password &&
              Boolean(registerForm.errors.password)
            }
            value={registerForm.values.password}
            onChange={registerForm.handleChange}
            helperText={
              registerForm.touched.password && registerForm.errors.password
            }
            name="password"
            id="password"
            fullWidth
            variant="outlined"
            placeholder="Password"
            className={classes.root}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <img src={keypng} alt="key" />
                </InputAdornment>
              ),
            }}
          />
          <div className="auth__bottom">
            <p className="auth__terms">
              By creating an account, you agree with the Terms, Conditions and
              Privacy Policy
            </p>
            <div className="auth__checkbox">
              <input id="checkbox" type="checkbox"></input>
              <label for="checkbox"> Subscribe to Newsletter</label>
            </div>
            <input
              type="submit"
              value="Sign Up"
              className="input-submit"
            ></input>
            <p className="auth__redirect">
              Already have an account?{" "}
              <a className="auth__redirect-login" href="login">
                Sign in
              </a>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignUpPage;
