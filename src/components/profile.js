import { useHistory } from "react-router-dom";
import Navigation from "./navigation";
import "./profile.css";

const Profile = () => {
  const history = useHistory();
  const logoutFunction = async () => {
    await localStorage.clear();
    history.push("/auth/login");
  };

  return (
    <div className="profile">
      <Navigation />
      <div className="profile__main">
        <header className="profile__header">
          <span className="header__title">MY PROFILE</span>
          <div className="profile-header__left">
            <button className="sign-out-btn" onClick={logoutFunction}>
              SIGN OUT
            </button>
            <div className="help-menu">
              <button className="help-menu-btn">
                <span>?</span>
              </button>
            </div>
          </div>
        </header>
        <div className="profile__account">
          <div className="account__header">
            <div className="account__navigation">
              <ul>
                <li className="active-nav">PROFILE</li>
                <li>BILLING</li>
                <li className="invoice-history">INVOICE HISTORY</li>
              </ul>
            </div>
            <button className="account-deactivate-btn">
              DEACTIVATE ACCOUNT
            </button>
          </div>
          <div className="account__main">
            <div className="main__wrapper">
              <div className="account__main-left">
                <span className="account__section-title">PROFILE PICTURE</span>
                <div className="account__profile-wrapper account__base-size">
                  <div className="account__profile-cont">
                    <img
                      src="https://app.365dropship.com/assets/images/profile-example.jpg"
                      alt="profileImage"
                    />
                  </div>
                  <button>Upload</button>
                </div>
                <span className="account__section-title">CHANGE PASSWORD</span>
                <div className="account__password-wrapper account__base-size">
                  <div className="field-input ">
                    <span>Current field</span>
                    <input type="text"></input>
                  </div>
                  <div className="field-input">
                    <span>New Password</span>
                    <input type="text"></input>
                  </div>
                  <div className="field-input">
                    <span>Confirm New Password</span>
                    <input type="text"></input>
                  </div>
                </div>
              </div>
              <div className="account__main-right">
                <span className="account__section-title">PERSONAL DETAILS</span>
                <div className="account__details-wrapper account__base-size">
                  <div className="field-input">
                    <span>First Name</span>
                    <input type="text"></input>
                  </div>
                  <div className="field-input">
                    <span>Last Name</span>
                    <input type="text"></input>
                  </div>
                  <div className="field-input">
                    <span>Country</span>
                    <input type="text"></input>
                  </div>
                </div>
                <span className="account__section-title">
                  CONTACT INFORMATION
                </span>
                <div className="account__contacts-wrapper account__base-size">
                  <div className="field-input">
                    <span>Email</span>
                    <input type="text"></input>
                  </div>
                  <div className="field-input">
                    <span>Skype</span>
                    <input type="text"></input>
                  </div>
                  <div className="field-input">
                    <span>Phone</span>
                    <input type="text"></input>
                  </div>
                </div>
              </div>
            </div>
            <div className="main__btn-container">
              <button>Save Changes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
