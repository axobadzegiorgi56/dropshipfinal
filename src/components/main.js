import "./main.css";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
const Main = () => {
  let history = useHistory();

  const useStyles = makeStyles({
    root: {
      background: "#61d5df",
      textDecoration: "none",
      border: 0,
      borderRadius: 4,
      color: "white",
      padding: "15px 30px",
      width: "180px",
      height: "52px",
      marginTop: "20px",
      fontFamily: "Gilroy",
      fontWeight: "500",
      "&:hover": {
        backgroundColor: "#61d5df",
      },
    },
  });

  const classes = useStyles();

  const loginBtn = () => {
    if (localStorage.getItem("token")) {
      history.push("/catalog");
    } else {
      history.push("/auth/login");
    }
  };

  const signupBtn = () => {
    history.push("/auth/signup");
  };

  return (
    <div className="main">
      <header className="main__header">
        <img
          className="main__header-logo"
          src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-30.png"
          alt="logo"
        />
        <ul className="main__header-nav">
          <li className="nav__item">ABOUT</li>
          <li className="nav__item">CATALOG</li>
          <li className="nav__item">PRICING</li>
          <li className="nav__item">SUPPLIERS</li>
          <li className="nav__item">HELP CENTER</li>
          <li className="nav__item">BLOG</li>
          <li className="nav__item" onClick={signupBtn}>
            SIGN UP NOW
          </li>
          <li className="nav__item" onClick={loginBtn}>
            LOGIN
          </li>
        </ul>
      </header>
      <div className="main__container">
        <div className="main__content">
          <img
            className="main__content-logo"
            src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
            alt="logo"
          />
          <h4>WE GOT YOUR SUPPLY CHAIN COVERED</h4>
          <h4>365 DAYS A YEAR!</h4>
        </div>
      </div>
      <Link to="auth/signup">
        <Button className={classes.root} variant="contained" color="primary">
          Sign Up Now
        </Button>
      </Link>
    </div>
  );
};

export default Main;
