import axios from "axios";

export const BASE_URL = "http://18.185.148.165:3000";

axios.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const productsFetch = async () => {
  const res = await axios.get(BASE_URL + "/api/v1/products");
  return res.data.data;
};
export const addProduct = async (data) => {
  const result = await axios.post(BASE_URL + "/api/v1/products", data);
  return result.data.data;
};

export const addToCart = async (productId, qty) => {
  const res = await axios.post(BASE_URL + "/api/v1/cart/add", {
    productId,
    qty,
  });
  return res.data.data;
};

export const deleteProductAPI = async (productId) => {
  const res = await axios.delete(BASE_URL + `/api/v1/products/${productId}`);
  return res.data.data;
};

export const singleProduct = async (id) => {
  const result = await axios.get(BASE_URL + `/api/v1/products/${id}`);
  return result.data.data;
};
export const updateProduct = async (id, data) => {
  const result = await axios.put(BASE_URL + `/api/v1/products/${id}`, data);
  return result.data.data;
};

export const cartFetch = async () => {
  const items = await axios.get(BASE_URL + "/api/v1/cart");
  console.log(items);
  return items.data.data.cartItem.items;
};

export const removeFromCart = async (id) => {
  try {
    await axios.post(BASE_URL + `/api/v1/cart/remove/${id}`);
  } catch (err) {
    alert(err);
  }
};

export const login = async (email, password) => {
  try {
    await axios.post(BASE_URL + "/login", { email, password }).then((user) => {
      localStorage.setItem("user", JSON.stringify(user));
      localStorage.setItem("token", user.data.data.token);
    });
  } catch (err) {
    throw new Error(err);
  }
};
export const signUp = async (
  firstName,
  lastName,
  email,
  password,
  passwordConfirmation
) => {
  try {
    await axios
      .post(BASE_URL + "/register", {
        firstName,
        lastName,
        email,
        password,
        passwordConfirmation,
      })
      .then((user) => {
        localStorage.setItem("user", JSON.stringify(user));
        localStorage.setItem("token", user.data.data.token);
      });
  } catch (err) {
    throw new Error(err);
  }
};
