import "./navigation.css";
import cartSVG from "../icons/Cart.svg";
import catalogSVG from "../icons/Catalog.svg";
import dashboardSVG from "../icons/Dashboard.svg";
import inventorySVG from "../icons/Inventory.svg";
import ordersSVG from "../icons/Orders.svg";
import profilePNG from "../icons/profile.png";
import storeslistSVG from "../icons/StoresList.svg";
import transactionsSVG from "../icons/Transactions.svg";
import { useHistory } from "react-router-dom";

const Navigation = () => {
  const history = useHistory();

  const catalogPush = () => {
    history.push("/catalog");
    console.log("click");
  };

  const cartPush = () => {
    history.push("/cart");
  };

  const profilePush = () => {
    history.push("/profile");
  };

  return (
    <div className="navigation">
      <div className="nav__logo-container">
        <img
          className="navigation__logo"
          src="https://app.365dropship.com/assets/images/dropship_logo.png"
          alt="logo"
        />
      </div>

      <div className="nav__item-container" onClick={profilePush}>
        <img className="nav-profile" src={profilePNG} alt="profile" />
      </div>
      <div className="nav__item-container">
        <img className="nav-item" src={dashboardSVG} alt="dashboard" />
      </div>
      <div className="nav__item-container" onClick={catalogPush}>
        <img className="nav-item" src={catalogSVG} alt="catalog" />
      </div>
      <div className="nav__item-container">
        <img className="nav-item" src={inventorySVG} alt="inventory" />
      </div>
      <div className="nav__item-container" onClick={cartPush}>
        <img className="nav-item" src={cartSVG} alt="cart" />
      </div>
      <div className="nav__item-container">
        <img className="nav-item" src={ordersSVG} alt="orders" />
      </div>
      <div className="nav__item-container">
        <img className="nav-item" src={transactionsSVG} alt="transactions" />
      </div>
      <div className="nav__item-container">
        <img className="nav-item" src={storeslistSVG} alt="storelist" />
      </div>
    </div>
  );
};

export default Navigation;
