import "./auth.css";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import keypng from "../images/key-auth.PNG";
import mailpng from "../images/mail-auth.PNG";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { useEffect } from "react";
import axios from "axios";
import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";

const LogInValidation = yup.object({
  email: yup.string().required("Invalid Email").email("Invalid Email"),
  password: yup.string().required("Invalid Password"),
});

const LoginPage = () => {
  const BASE_URL = "http://18.185.148.165:3000";
  let history = useHistory();

  const useStyles = makeStyles({
    root: {
      borderRadius: 4,
      width: "331px",
      height: "48px",
      marginBottom: "30px",
    },
  });
  const classes = useStyles();

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      history.push("/catalog");
      console.log(token);
    }
  }, []);

  const checkToken = (history) => {
    const token = localStorage.getItem("token");
    if (token) {
      history.push("/catalog");
      console.log(token);
    }
  };

  const loginForm = useFormik({
    initialValues: { email: "", password: "" },
    onSubmit: (values) => {
      Login(values.email, values.password).then(() => checkToken(history));
    },
    validationSchema: LogInValidation,
  });

  const Login = async (email, password) => {
    try {
      const result = await axios.post(BASE_URL + "/login", {
        email: email,
        password: password,
      });
      if (result.data.success) {
        history.push("/cart");
        console.log(result);
        localStorage.setItem("user", JSON.stringify(result.data.data));
        localStorage.setItem(
          "token",
          JSON.stringify(result.data.data.token).replace(/['"]+/g, "")
        );
        localStorage.setItem("id", JSON.stringify(result.data.data.id));
      }
    } catch (err) {
      alert("something went wrong!");
    }
  };

  return (
    <div className="auth">
      <div className="auth__container-small">
        <div className="auth__heading">
          <div className="auth__logo">
            <img
              src="https://app.365dropship.com/assets/images/auth/logo.svg"
              alt="logo"
            />
          </div>
        </div>
        <p className="auth__title">Members Log In</p>
        <form className="auth__inputs" onSubmit={loginForm.handleSubmit}>
          <TextField
            name="email"
            id="email"
            error={loginForm.touched.email && Boolean(loginForm.errors.email)}
            value={loginForm.values.email}
            onChange={loginForm.handleChange}
            helperText={loginForm.touched.email && loginForm.errors.email}
            fullWidth
            variant="outlined"
            placeholder="E-mail"
            className={classes.root}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <img src={mailpng} alt="mail" />
                </InputAdornment>
              ),
            }}
          />

          <TextField
            name="password"
            id="password"
            error={
              loginForm.touched.password && Boolean(loginForm.errors.password)
            }
            value={loginForm.values.password}
            onChange={loginForm.handleChange}
            helperText={loginForm.touched.password && loginForm.errors.password}
            type="password"
            fullWidth
            variant="outlined"
            placeholder="Password"
            className={classes.root}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <img src={keypng} alt="key" />
                </InputAdornment>
              ),
            }}
          />
          <div className="auth__bottom">
            <p className="auth__forgot-password">Forgot Password?</p>
            <input
              type="submit"
              className="input-submit"
              value="Log In"
            ></input>
            <p className="auth__redirect">
              Don't have an account?{" "}
              <a className="auth__redirect-login" href="signup">
                Sign Up
              </a>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
